const errorMsg = "Input provided is not a valid name. No special characters, and minimum of 3 letters long.";
const errorElem = document.getElementById('errorElem');
const gcashForminput = document.getElementById('gcashForm_input');

function handleKeyPress() { // handle input onkeypress event
	errorElem.style.display = "none";
}

function throwError() { // error fn declaration
	errorElem.style.display = "block";
	gcashForminput.value = "";
	gcashForminput.focus();
	errorElem.innerText = errorMsg;
}

document.getElementById('gcashForm').addEventListener('submit', function(e) { // bind submit event to the form and pass the data to hideName fn
	e.preventDefault();

	if (!gcashForminput.value.length) {
		gcashForminput.focus();
		errorElem.innerText = errorMsg;
	}
	
	hideName(gcashForminput.value);
})

function hideName(name) {
	
	let firstNameArr = [];
	const regTest = /^[A-Za-z\s]*$/; // ONLY letters and one space per word

	// checks name if: has value, only letters and spaces, more than 1 word
	if (regTest.test(name) === false || name.length < 3 || name.split(" ").length <= 1) {
		return throwError();
	}
	
	let nameArr = name.split(" ");
	let lastName = nameArr[nameArr.length-1].substring(0,1).toUpperCase() + "."

	// loop thru each word in name
	for (let i=0; i<=nameArr.length-2; i++) {
		let letterLen = 2;
		
		if (nameArr[i].length > 4) { // checks name if: has 4 or more letters. Format: Donna > Do**a
			letterLen = 2;
		} else if (nameArr[i].length === 3) { // checks name if: has 3 letters. Format: Ava > A*a
			letterLen = 1;
		} else { // fall back: any of the name is NOT greater than 4 NOR equals to 3 letters
			return throwError();
		}
		
		// get the letters (to be replaced by asterisk) based on the given index
		let lettersToAsterisk = nameArr[i].substring(letterLen, nameArr[i].length-1);

		// find and replace the letters with asterisk/s, then push them to the firstNameArr
		firstNameArr.push(nameArr[i].replace(lettersToAsterisk, "*".repeat(lettersToAsterisk.length)).toUpperCase());
	}

	// assemble the result
	let firstName = firstNameArr.toString().replaceAll(",", " ");
	let result =  `${firstName} ${lastName}`;
	document.getElementById('avatarElem').src = `https://ui-avatars.com/api/?size=100&rounded=true&bold=true&background=random&name=${firstNameArr[0]}+${lastName}`
	
	gcashForminput.value = ""; // empty the input form
	handleKeyPress(); // hides the error element

	return document.getElementById("hideName").innerHTML = result;
}
